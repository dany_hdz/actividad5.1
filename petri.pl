/* Actividad 5.1: Programación lógica
   Implementación de métodos computacionales TC2037.1
   Daniela Hernández y Hernández    A01730397
   21/04/2021 */

% -------- Parte 1: Codificación de una Red de Petri y predicados básicos --------
% Transciones
tr(a).
tr(b).
tr(c).
tr(d).
tr(e).

% Lugares
pl(p0).
pl(p1).
pl(p2).
pl(p3).
pl(p4).
pl(p5).

% Arcos
arc(p0, a).
arc(a, p1).
arc(a, p2).
arc(p1, b).
arc(p1, d).
arc(p2, c).
arc(p2, d).
arc(b, p3).
arc(d, p3).
arc(c, p4).
arc(d, p4).
arc(p3, e).
arc(p4, e).
arc(e, p5).

% Marcado inicial
m0(p0).

% Obtener preset y poset
% preset(d, R).
preset(Node, R) :- setof(X, arc(X, Node), R).
poset(Node, R) :- setof(X, arc(Node, X), R).

% Verificar que una transición se pueda ejecutar dado un marcado
% isEnabled(b,[p1,p4]).
isEnabled(Trans, Marcado) :- tr(Trans), preset(Trans, R), subset(R, Marcado).

% Función fire
% Fire(a, [p0], Nm).
fire(Trans, Marcado, Nm) :- isEnabled(Trans, Marcado) -> 
    preset(Trans, Pre), subtract(Marcado, Pre, N), poset(Trans, Pos), append(N, Pos, Nm);
    Nm = Marcado.

% -------- Parte 2: Enablement --------------------------------------------------
% enablement([p0], E).
enablement(Marcado, Enabled) :- findall(R, isEnabled(R, Marcado), Enabled).

% -------- Parte 3: Replay --------------------------------------------------
% replay([a,b,c,e]).
replay([],_).
replay([H|T], Marcado) :- isEnabled(H, Marcado), fire(H, Marcado, N), replay(T, N).
replay(Traza) :- m0(M), replay(Traza, [M]).
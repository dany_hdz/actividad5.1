/* Actividad 5.1: Programación lógica
   Implementación de métodos computacionales TC2037.1
   Daniela Hernández y Hernández    A01730397
   21/04/2021 */

% VERSIÓN 2
% -------- Parte 1: Codificación de una Red de Petri y predicados básicos --------
% Transciones
tr(a).
tr(b).
tr(c).
tr(d).
tr(e).

% Lugares
pl(p0).
pl(p1).
pl(p2).
pl(p3).
pl(p4).
pl(p5).

% Arcos
arc(p0, a).
arc(a, p1).
arc(a, p2).
arc(p1, b).
arc(p2, c).
arc(b, p3).
arc(c, p4).
arc(p4, d).
arc(d, p2).
arc(p3, e).
arc(p4, e).
arc(e, p5).

% Marcado inicial
m0([p0]).

% Obtener preset y poset
preset(Node, R) :- setof(X, arc(X, Node), R).
poset(Node, R) :- setof(X, arc(Node, X), R).

% Verificar que una transición se pueda ejecutar dado un marcado
isEnabled(Trans, Marcado) :- tr(Trans), preset(Trans, R), subset(R, Marcado).

% Función fire
fire(Trans, Marcado, Nm) :- isEnabled(Trans, Marcado) -> 
    preset(Trans, Pre), subtract(Marcado, Pre, N), poset(Trans, Pos), append(N, Pos, Nm);
    Nm = Marcado.

% -------- Parte 2: Enablement --------------------------------------------------
enablement(Marcado, Enabled) :- findall(R, isEnabled(R, Marcado), Enabled).

% -------- Parte 3: Replay --------------------------------------------------
% replay([a,b,c,e]).
replay([],_).
replay([H|T], Marcado) :- isEnabled(H, Marcado), fire(H, Marcado, N), replay(T, N).
replay(Traza) :- m0(M), replay(Traza, M).